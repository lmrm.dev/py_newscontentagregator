from django.utils import timezone
from django.test import TestCase
from django.urls.base import reverse

from news.models import News

class NewsTests(TestCase):
    def setUp(self):
        self.news = News.objects.create(
            title = 'Champions league: Sporting CP won the competition',
            subtitle = 'This could be an amazing news. Instead is a lie',
            image = 'https://coolimage.com',
            url = 'https://myawesomeshow.com',
            pub_date = timezone.now(),
            content_description = '''This should contain a lot of text. 
                I should be able to say what i want right here i i choose not to do so. 
                I must remain in silence and ... I think that this is enough. Thank you 
                for reading this. Bisus.
                ''',
            guid = 'de194720-7b4c-49e2-a05f-432436d3fetr'
    )

    def true_test_news_content(self):
        self.assertEqual(self.news.content_description, '''This should contain a lot of text. 
                I should be able to say what i want right here i i choose not to do so. 
                I must remain in silence and ... I think that this is enough. Thank you 
                for reading this. Bisus.
                ''')
        self.assertEqual(self.news.title, 'Champions league: Sporting CP won the competition')
        self.assertEqual(self.news.url, 'https://myawesomeshow.com')

    def true_test_news_string(self):
        self.assertEqual(str(self.news), 'My Python News: My Awesome news')
    
    def test_home_page_status_code(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code,200)
    
    def test_home_page_uses_correct_template(self):
        response = self.client.get(reverse("homepage"))
        self.assertTemplateUsed(response,"homepage.html")

    def test_homepage_list_contents(self):
        response = self.client.get(reverse("homepage"))
        self.assertContains(response, "News! Logo")