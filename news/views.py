from django.shortcuts import render

from django.views.generic import ListView

from .models import News

class HomePageView(ListView):
    template_name = "homepage.html"
    model = News

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["news"] = News.objects.filter().order_by("-pub_date")[:10]
        return context


'''
In the code above, you make use of a class-based view to send the news to the homepage:
    - Line 7: You inherit from the ListView class so that you can iterate over the news. By default, 
    it will iterate over all news as defined by model = Episode on line 9.
    - Lines 11 to 14: You override the context data and filter by the ten most recent news, as 
    determined by the published date, pub_date. You want to filter here because, otherwise, there could 
    be hundreds—if not thousands—of news to pass to the homepage.
'''