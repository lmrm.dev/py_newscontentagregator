from email.mime import image
from pyexpat import model
from statistics import mode
from django.db import models

class News(models.Model):
    title = models.CharField(max_length=200)
    subtitle = models.CharField(max_length=1000)
    image = models.URLField()
    url = models.URLField()
    pub_date = models.DateField()
    content_description = models.TextField()
    guid = models.CharField(max_length=50)

    def __str__(self) -> str:
        return f"{self.podcast_name}: {self.title}"
