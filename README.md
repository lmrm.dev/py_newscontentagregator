# **Goal**

This project main goal is to create a platform capable of retrieve news data from specific topics from a free API and show them in a more compact and organized way in a web platform.
All of this will be implemented using pythhon and its packages such as Django.
This project development will be based on a similar project implementation from [realpython](https://realpython.com/build-a-content-aggregator-python/). The main difference is that instead of retrieving podcasts from the web this will get news.

# **Virtual environment**

It was created an virtual environment for this project at the local machine. It is called "canews-env".
The knowledge for the virtual environment implementation was provided by the [python.org platform](https://docs.python.org/3/tutorial/venv.html)
The steps are the following (The command line must be initiated as administrator):
- Check if the location of the command prompt is set to the projects root folder. In this case was the news folder;
- python3 -m venv tutorial-env: tutorial-env is the name of the virtual environment. Can give it anything we want;
- tutorial-env\Scripts\activate.bat: Activates the virtual environment called tutorial-env;
- Manage packages (This can be made by several ways. Here are some):
    - python -m pip install requests==2.6.0: Installing an specific version of a package called "requests";
    - python -m pip install -r requirements.txt: Install specific package versions based on a requirements.txt file. This file should contain data in the following format: "Django==3.2.6\ndjango-apscheduler==0.6.0".
    - For more information about pip and package management see the link above from the python.org platform.


# **Set up Django and start building**

To complete this step of the build, you need to do the following four things:
- Create a Django project in the current working directory, /news: ***django-admin startproject content_agregator .***
- Create a news Django app: ***python manage.py startapp news***
- Run initial migrations: makemigrations is responsible for packaging up your model changes into individual migration files - analogous to commits - and migrate is responsible for applying those to your database. ***python manage.py makemigrations && python manage.py migrate***
- Create a superuser: Creat a user who can login to the admin site. ***python manage.py createsuperuser***

After that, add the recently intalled app in the project settings. Go to content_agregator -> settings.py -> INSTALLED_APPS = [..., 'news.apps.NewsConfig']

To a more in depth knowledge, check this [django tutorial](https://docs.djangoproject.com/en/1.8/intro/tutorial01/).

### Start the django server

Start the Django server: ***python manage.py runserver***. Then, in the search bar of your browser, place "localhost:8000".

# Create models

Once this is an news platform, the data that were are trying to show is an news model. This News model shouldn’t only reflect the information you’d like to capture as the developer. It should also reflect the information that the user would like to see.
Before start coding, is importing to define a list of requirements of the app:

> As a user, I would like to:
> 
>- Know the title of an news
>- Read a description of the news
>- Know when an news was published
>- Have a clickable URL so I can be redirected to the news provider
>- See an image of the news
>- See the news content
> 
> As a developer, I would like to:
> 
>- Have a uniquely identifiable attribute for each news so I can avoid duplicating it in the database

After that, you can start modeling in python code. Check the news/models.py file to see how it was done.

Django has one feature that facilitates developers and data management. This allows the admin to interact with the data directly in the database. For that, the code in the news/admin.py should be replaced. Check it out.

Now, you should define an autoincrement primary key at the news/app.py file. The 'BigAutoField' should be replaced by 'AutoField' in order to fix an error. This will make sure that all the models have an primary key added automatically.

# Database migrations
Migrations are Django's way of propagating changes made in models (adding a field, deleting a model, etc.) into your database schema. Its like updating your the database after an model's change.

You can now run the Django migrations to include your Episode table in the database:
- python manage.py makemigrations: Makes the migrations files;
- python manage.py migrate: Executes the migrations in the database.

# Unit testing

It could have bee used the pytest library, but, in this tutorial, this application will be testes using Django’s built-in testing framework for your unit tests.
Were implemented the following steps:
- Createan example _News_ object;
- Implement the unit tests creating methods within the NewsTests class and using assert method;
- Run the command ***python manage.py test***.
For more information, check the news/tests.py.

# HTML & CSS

It were created two directories:
- static:
- template: Both this folders must be listed at the settings.py of the content_agregator folder in order to django know them.

The news/views.py class was inherit from the ListView class and the underlying method get_context_data. This way we can filter some unecessary data.

### URLs

To identify all the path in our application its mandatory to create an file (in our case was the url.py in hte news folder) with a list of paths. Each element assigns a class view to an name. This will then build the full URL.
To make the content agregator app know about all the path within the app, in the content_agregator/url.py should be added an path that includes all the news urls.

Now, test your app. Just as it was done previously:
- python manage.py runserver;
- place _localhost:8000_ at the browsers search bar.
